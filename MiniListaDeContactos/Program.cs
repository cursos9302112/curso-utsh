﻿using COMMON.Entidades;
using DAL;
using System.Dynamic;

namespace MiniListaDeContactos
{
    internal class Program
    {
        static RepositorioPersonas repo;
        static void Main(string[] args)
        {
            repo=new RepositorioPersonas();
            string opcion = "";
            do
            {
                Console.Clear();
                Console.WriteLine("Mini lista de contactos");
                Console.WriteLine("1. Agregar contacto");
                Console.WriteLine("2. Listar contactos");
                Console.WriteLine("S. Salir");
                Console.Write("Opción: ");
                opcion = Console.ReadLine();
                switch (opcion)
                {
                    case "1":
                        Agregar();
                        break;
                    case "2":
                        Listar();
                        break;
                    case "S":
                        Console.WriteLine("Gracias por usar el programa!!!");
                        Console.ReadLine();
                        break;

                    default:
                        Console.WriteLine("Opción incorrecta...");
                        Console.ReadLine();
                        break;
                }
            }while (opcion!="S");
            
        }

        private static void Listar()
        {
            Console.WriteLine("Id\tNombre\tTeléfono");
            foreach (var item in repo.Read)
            {
                Console.WriteLine($"{item.IdPersona}\t{item.Nombre}\t{item.Telefono}");
            }
            Console.WriteLine("Total de registros: " + repo.Read.Count);
            Console.ReadLine();
        }

        private static void Agregar()
        {
            Console.WriteLine("Introduce los datos de la nueva persona:");
            Persona persona = new Persona();
            Console.Write("Nombre: ");
            persona.Nombre = Console.ReadLine();
            Console.Write("Teléfono: ");
            persona.Telefono = Console.ReadLine();
            if (repo.Create(persona))
            {
                Console.WriteLine("Persona agregada correctamente...");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine($"Error: {repo.Error}");
                Console.ReadLine();
            }
        }
    }
}