﻿using COMMON.Entidades;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PuntoDeVentaEscritorio
{
    /// <summary>
    /// Lógica de interacción para Categorias.xaml
    /// </summary>
    public partial class Categorias : UserControl
    {
        RepositorioGenerico<Categoria> repositorioCategoria;
        public Categorias()
        {
            InitializeComponent();
            repositorioCategoria = new RepositorioGenerico<Categoria>();
            if (repositorioCategoria.Read.Count == 0)
            {
                repositorioCategoria.Create(new Categoria()
                {
                    Nombre = "Abarrotes"
                });
                repositorioCategoria.Create(new Categoria()
                {
                    Nombre = "Refrescos"
                });
                repositorioCategoria.Create(new Categoria()
                {
                    Nombre = "Botanas"
                });
            }

            dtgCategorias.ItemsSource = repositorioCategoria.Read;
        }
    }
}
