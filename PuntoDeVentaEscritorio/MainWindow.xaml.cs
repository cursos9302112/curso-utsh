﻿using COMMON.Entidades;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PuntoDeVentaEscritorio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : UserControl
    {
        RepositorioPersonas repo;
        bool esNuevo = false;
        Persona personaTemp;
        public MainWindow()
        {
            InitializeComponent();
            repo= new RepositorioPersonas();
            ActualizarTabla();
            ModoEditar(false);
        }

        private void ModoEditar(bool v)
        {
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnEliminar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnActualizar.IsEnabled = !v;
            cajas.IsEnabled = v;
        }

        private void LimpiarCajas()
        {
            txbId.Clear();
            txbNombre.Clear();
            txbTelefono.Clear();
        }

        private void ActualizarTabla()
        {
            grdContactos.ItemsSource = null;
            grdContactos.ItemsSource = repo.Read;
            LimpiarCajas();

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo= true;
            personaTemp= new Persona();
            ModoEditar(true);
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            personaTemp = grdContactos.SelectedItem as Persona;
            if(personaTemp != null)
            {
                //si se selecciono una persona
                esNuevo = false;
                ModoEditar(true);
                txbId.Text = personaTemp.IdPersona.ToString();
                txbNombre.Text = personaTemp.Nombre;
                txbTelefono.Text= personaTemp.Telefono;
            }
            else
            {
                //no se selecciono una persona
                MessageBox.Show("Primero selecciona un contacto", "Lista de contactos", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            personaTemp = grdContactos.SelectedItem as Persona;
            if (personaTemp != null)
            {
                //si se selecciono una persona
                if (MessageBox.Show("Realmente deseas eliminar el contacto?", "Mini lista de contactos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (repo.Delete(personaTemp))
                    {
                        MessageBox.Show("Registro eliminado correctamente", "Mini lista de contactos", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show(repo.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                personaTemp = null;
            }
            else
            {
                //no se selecciono una persona
                MessageBox.Show("Primero selecciona un contacto", "Lista de contactos", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            personaTemp.Nombre = txbNombre.Text;
            personaTemp.Telefono = txbTelefono.Text;

            bool r=esNuevo?repo.Create(personaTemp):repo.Update(personaTemp);
            if (r)
            {
                MessageBox.Show("Registro guardado correctamente", "Lista de contactos", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();
                ModoEditar(false);
            }
            else
            {
                MessageBox.Show(repo.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarTabla();
        }
    }
}
