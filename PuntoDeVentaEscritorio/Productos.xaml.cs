﻿using COMMON.Entidades;
using COMMON.Interfaces;
using COMMON.Modelos;
using DAL;
using DALSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PuntoDeVentaEscritorio
{
    /// <summary>
    /// Lógica de interacción para Productos.xaml
    /// </summary>
    public partial class Productos : UserControl
    {
        IRepositorioGenerico<Producto> repositorioProductos;
        RepositorioGenerico<Categoria> repositorioCategorias;
        List<Categoria> categorias;
        Producto producto;
        bool esEdicion;
        public Productos()
        {
            InitializeComponent();
            repositorioCategorias = new RepositorioGenerico<Categoria>();
            repositorioProductos = new RepositorioProducto();
            esEdicion = false;
            ActualizarTabla();
        }

        private void ActualizarTabla()
        {
            grdTabla.ItemsSource = null;
            //grdTabla.ItemsSource = repositorioProductos.Read;


            var productos = repositorioProductos.Read;
            categorias= repositorioCategorias.Read;
            cmbCategorias.ItemsSource = categorias;
            List<ProductosModel> datos= new List<ProductosModel>();
            foreach(var p in productos)
            {
                datos.Add(new ProductosModel( p, categorias.Where(c => c.Id == p.IdCategoria).SingleOrDefault()));
            }
            grdTabla.ItemsSource = datos;
            HabilitarCajas(false);
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            producto = new Producto();
            HabilitarCajas(true);
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            ProductosModel model = grdTabla.SelectedItem as ProductosModel;
            if(model != null)
            {
                producto = model.ObtenerProducto();
                esEdicion = true;
                HabilitarCajas(true);
                txbNombre.Text = model.Nombre;
                txbPrecio.Text = model.Precio.ToString();
                cmbCategorias.SelectedItem =categorias.Where(c=>c.Id== model.ObtenerCategoria().Id).SingleOrDefault();
            }
            else
            {
                MessageBox.Show("No se ha seleccionado un elemento","Productos", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void HabilitarCajas(bool v)
        {
            cmbCategorias.IsEnabled = v;
            txbNombre.IsEnabled = v;
            txbPrecio.IsEnabled = v;
            if(!v)
            {
                txbNombre.Clear();
                txbPrecio.Clear();
                cmbCategorias.SelectedItem=null;
            }
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnCancelar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnActualizar.IsEnabled = !v;

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            ProductosModel model = grdTabla.SelectedItem as ProductosModel;
            if (model != null)
            {
                if(MessageBox.Show($"¿Realmente desea eliminar el producto {model.Nombre}?","Productos",MessageBoxButton.YesNo, MessageBoxImage.Question)== MessageBoxResult.Yes)
                {
                    if(repositorioProductos.Delete(model.Id))
                    {
                        MessageBox.Show("Producto eliminado correctamente", "Productos", MessageBoxButton.OK, MessageBoxImage.Information);
                        HabilitarCajas(false);
                    }
                    else
                    {
                        MessageBox.Show(repositorioProductos.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("No se ha seleccionado un elemento", "Productos", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Categoria categoria = cmbCategorias.SelectedItem as Categoria;
            if(categoria != null)
            {

                producto.IdCategoria = categoria.Id;
                producto.Nombre = txbNombre.Text;
                producto.Precio = float.Parse(txbPrecio.Text);
                Producto r=esEdicion?repositorioProductos.Update(producto):repositorioProductos.Create(producto);
                if(r != null)
                {
                    MessageBox.Show("Producto correctamente guardado","Productos", MessageBoxButton.OK, MessageBoxImage.Information);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show(repositorioProductos.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
            ActualizarTabla();
        }

        private void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarTabla();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
        }
    }
}
