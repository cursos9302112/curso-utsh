﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeVentaEscritorio
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            contenedor.Children.Clear();
            contenedor.Children.Add(mainWindow);
        }

        private void menuPersonas_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void menuProductos_Click(object sender, RoutedEventArgs e)
        {
            Productos productos = new Productos();
            contenedor.Children.Clear();
            contenedor.Children.Add(productos);
        }

        private void menuCategorias_Click(object sender, RoutedEventArgs e)
        {
            Categorias categorias = new Categorias();
            contenedor.Children.Clear();
            contenedor.Children.Add(categorias);
        }

        private void menuReporte_Click(object sender, RoutedEventArgs e)
        {
            ReporteVentas reporteVentas = new ReporteVentas();
            contenedor.Children.Clear();
            contenedor.Children.Add(reporteVentas);
        }
    }
}
