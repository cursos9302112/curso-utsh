﻿using DALSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeVentaEscritorio
{
    /// <summary>
    /// Lógica de interacción para ReporteVentas.xaml
    /// </summary>
    public partial class ReporteVentas : UserControl
    {
        RepositorioReporteVenta repo;
        public ReporteVentas()
        {
            InitializeComponent();
            repo = new RepositorioReporteVenta();
            dtgReporte.ItemsSource = repo.ObtenerReporte();
        }
    }
}
