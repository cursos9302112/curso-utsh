﻿using COMMON.Entidades;

namespace COMMON.Interfaces
{
    public interface IRepositorioGenerico<T> where T : Base
    {
        string Error { get; }
        List<T> Read { get; }

        T Create(T item);
        bool Delete(string id);
        T GetById(string id);
        T Update(T item);
    }
}