﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Persona
    {
        public string Id { get; set; }    
        public int IdPersona { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
    }
}
