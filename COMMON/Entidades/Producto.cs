﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Producto:Base
    {
        public string IdCategoria { get; set; }
        public string Nombre { get; set; }
        public float Precio { get; set; }
    }
}
