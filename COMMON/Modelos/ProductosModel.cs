﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ProductosModel
    {
        Producto Producto;
        Categoria Categoria;
        public string Id { get; set; }
        public string NombreCategoria { get; set; }
        public string Nombre { get; set; }
        public float Precio { get; set; }
        public ProductosModel(Producto p, Categoria c)
        {
            Producto = p;
            Categoria = c;
            Id=p.Id;
            NombreCategoria = c.Nombre;
            Nombre=p.Nombre;
            Precio=p.Precio;
        }

        public Producto ObtenerProducto()
        {
            return Producto;
        }

        public Categoria ObtenerCategoria()
        {
            return Categoria;
        }
    }
}
