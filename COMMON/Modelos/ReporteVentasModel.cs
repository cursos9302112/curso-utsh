﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ReporteVentasModel
    {
        public int IdVenta { get; set; }
        public DateTime FechaHora { get; set; }
        public string Comprador { get; set; }
        public int Cantidad { get; set; }
        public string Nombre { get; set; }
        public float Precio { get; set; }
        public float Importe { get; set; }
    }
}
