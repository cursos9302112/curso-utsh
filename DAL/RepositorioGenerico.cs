﻿using COMMON.Entidades;
using COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class RepositorioGenerico<T> : IRepositorioGenerico<T> where T : Base
    {
        string ruta = "C:\\CursoNet\\PuntoDeVenta\\Contactos2.db";
        public string Error { get; set; }
        public RepositorioGenerico()
        {
            Error = "";
        }

        public List<T> Read
        {
            get
            {
                List<T> list;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    list = col.FindAll().ToList();
                }
                return list;
            }
        }

        public T GetById(string id)
        {
            try
            {
                T item;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    item = col.Find(p => p.Id == id).SingleOrDefault();
                }

                if (item == null)
                {
                    Error = "El registro no existe";
                    return null;
                }
                else
                {
                    Error = "";
                    return item;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }


        public T Create(T item)
        {
            try
            {
                item.Id = Guid.NewGuid().ToString();
                item.FechaHora = DateTime.Now;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    col.Insert(item);
                }
                Error = "";
                return item;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                bool r;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    r = col.Delete(id);
                }
                Error = "";
                return r;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public T Update(T item)
        {
            item.FechaHora = DateTime.Now;
            try
            {
                bool r;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    r = col.Update(item);
                }
                Error = "";
                return r ? item : null;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

    }
}
