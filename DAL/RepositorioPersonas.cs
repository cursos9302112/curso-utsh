﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using LiteDB;

namespace DAL
{
    /// <summary>
    /// Repositorio de Personas
    /// </summary>
    public class RepositorioPersonas
    {
        //ABC->CRUD->Create (insert), Read (Select), Update, Delete
        List<Persona> personas;
        Random r;
        string ruta = "C:\\CursoNet\\PuntoDeVenta\\Contactos.db";
        public RepositorioPersonas()
        {
            personas = new List<Persona>();
            r = new Random(DateTime.Now.Millisecond);
            Error = "";
        }
        /// <summary>
        /// Obtiene el error de la ultima operación ejecutada en el repositorio
        /// </summary>
        public string Error { get; private set; }
        public List<Persona> Read
        {
            get
            {
                List<Persona> personas;
                using (var db=new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<Persona>("Personas");
                    personas = col.FindAll().ToList();
                }
                return personas;
            }
        }
        /// <summary>
        /// Obtiene la persona con el Id Especificado
        /// </summary>
        /// <param name="id">Id de la persona a obtener</param>
        /// <returns>Persona</returns>
        public Persona GetById(int id)
        {
            try
            {
                Persona persona;
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<Persona>("Personas");
                    persona = col.Find(p => p.IdPersona == id).SingleOrDefault();
                }

                if(persona == null)
                {
                    Error = "El registro no existe";
                    return null;
                }
                else
                {
                    Error = "";
                    return persona;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Create(Persona persona)
        {
            try
            {
                persona.IdPersona = r.Next(0, 10000);
                persona.Id=Guid.NewGuid().ToString();   
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<Persona>("Personas");
                    col.Insert(persona);
                }
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool Delete(Persona persona)
        {
            try
            {
                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<Persona>("Personas");
                    col.Delete(persona.IdPersona);
                }
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error=ex.Message; 
                return false;
            }
        }

        public bool Update(Persona persona)
        {
            try
            {

                using (var db = new LiteDatabase(ruta))
                {
                    var col = db.GetCollection<Persona>("Personas");
                    col.Update(persona);
                }
                Error = "";
                return true;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

    }
}
