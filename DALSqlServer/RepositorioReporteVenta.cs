﻿using COMMON.Interfaces;
using COMMON.Modelos;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSqlServer
{
    public class RepositorioReporteVenta
    {
        string cadConexion = @"Data Source=localhost;Initial Catalog=CursoNET;Persist Security Info=True;User ID=sa;Password=*******;TrustServerCertificate=true";

        SqlConnection conexion;
        public string Error { get; private set; }
        public RepositorioReporteVenta()
        {
            conexion = new SqlConnection(cadConexion);
        }

        public List<ReporteVentasModel> ObtenerReporte()
        {
            try
            {
                List<ReporteVentasModel> datos = new List<ReporteVentasModel>();
                conexion.Open();

                SqlCommand cmd = new SqlCommand("select * from View_Reporte", conexion);
                SqlDataReader dr=cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReporteVentasModel model= new ReporteVentasModel();
                    model.IdVenta = dr.GetInt32(0);
                    model.FechaHora = dr.GetDateTime(1);
                    model.Comprador = dr.GetString(2);
                    model.Cantidad = dr.GetInt32(3);
                    model.Nombre = dr.GetString(4);
                    model.Precio = float.Parse(dr.GetDecimal(5).ToString());
                    model.Importe=float.Parse(dr.GetDecimal(6).ToString());
                    datos.Add(model);
                }
                dr.Close();
                conexion.Close();
                return datos;
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }
    }
}
