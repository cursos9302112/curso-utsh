﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALSqlServer
{
    public class RepositorioProducto : IRepositorioGenerico<Producto>
    {
        string cadConexion = @"Data Source=localhost;Initial Catalog=CursoNET;Persist Security Info=True;User ID=sa;Password=*******;TrustServerCertificate=true";
        

        SqlConnection conexion;

        public RepositorioProducto()
        {
            conexion = new SqlConnection(cadConexion);
        }

        public string Error { get; private set; }

        public List<Producto> Read
        {
            get
            {
                //select * from Productos
                try
                {
                    List<Producto> list = new List<Producto>();
                    conexion.Open();
                    SqlCommand cmd = new SqlCommand("Select * from productos", conexion);
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Producto producto = new Producto();
                        producto.Id = dataReader.GetString(0);
                        producto.FechaHora = dataReader.GetDateTime(1);
                        producto.Nombre = dataReader.GetString(3);
                        producto.IdCategoria = dataReader.GetString(2);
                        producto.Precio =float.Parse( dataReader.GetDecimal(4).ToString());
                        list.Add(producto);
                    }
                    dataReader.Close();
                    conexion.Close();
                    Error = "";
                    return list;
                }
                catch (Exception ex)
                {
                    if(conexion.State== System.Data.ConnectionState.Open)
                    {
                        conexion.Close();
                    }
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public Producto Create(Producto item)
        {
            try
            {
                conexion.Open();
                item.Id = Guid.NewGuid().ToString();
                item.FechaHora=DateTime.Now;
                //29/03/2023 11:54:30 am --> '2023/03/29 11:54:30'
                string fecha = $"{item.FechaHora.Year}/{item.FechaHora.Month}/{item.FechaHora.Day} {item.FechaHora.Hour}:{item.FechaHora.Minute}:{item.FechaHora.Second}";
                string fecha2 = item.FechaHora.ToString("yyyy/MM/dd HH:mm:ss");


                string sql = $"insert into productos (Id,FechaHora,IdCategoria,Nombre,Precio) values('{item.Id}','{fecha}','{item.IdCategoria}','{item.Nombre}',{item.Precio})";
                SqlCommand cmd = new SqlCommand(sql, conexion);
                int r=cmd.ExecuteNonQuery();//insert, update, delete
                conexion.Close();
                if (r > 0)
                {
                    Error = "";
                    return item;
                }
                else
                {
                    Error = "Error al insertar el registro...";
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                conexion.Open();
                string sql = $"delete from Productos where Id='{id}'";
                SqlCommand cmd = new SqlCommand(sql, conexion);
                int r = cmd.ExecuteNonQuery();//insert, update, delete
                conexion.Close();
                if (r > 0)
                {
                    Error = "";
                    return true;
                }
                else
                {
                    Error = "No existe dicho elemento";
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return false;
            }
        }

        public Producto GetById(string id)
        {
            try
            {
                conexion.Open();
                
                SqlCommand cmd = new SqlCommand($"select * from Productos where Id='{id}'", conexion);
                SqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.HasRows) //tiene filas? trajo el Id??   existe el id?
                {
                    Producto producto = new Producto();
                    while (dataReader.Read())
                    {
                        producto.Id = dataReader.GetString(0);
                        producto.FechaHora = dataReader.GetDateTime(1);
                        producto.Nombre = dataReader.GetString(3);
                        producto.IdCategoria = dataReader.GetString(2);
                        producto.Precio = dataReader.GetFloat(4);
                    }
                    dataReader.Close();
                    conexion.Close();
                    Error = "";
                    return producto;

                }
                else
                {
                    conexion.Close();
                    Error = "Elemento no existente";
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }

        public Producto Update(Producto item)
        {
            try
            {
                conexion.Open();
                //29/03/2023 11:54:30 am --> '2023/03/29 11:54:30'
                string fecha = item.FechaHora.ToString("yyyy/MM/dd HH:mm:ss");
                string sql = $"update Productos set FechaHora='{fecha}',Nombre='{item.Nombre}',IdCategoria='{item.IdCategoria}',Precio={item.Precio} where Id='{item.Id}'";
                SqlCommand cmd = new SqlCommand(sql, conexion);
                int r = cmd.ExecuteNonQuery();//insert, update, delete
                conexion.Close();
                if (r > 0)
                {
                    Error = "";
                    return item;
                }
                else
                {
                    Error = "Error al modificar el registro...";
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }
    }
}
